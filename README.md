## run first time

```bash
npm install 
npm start
```

## test

```bash
npm test
```

## references:

- [referencia de web-components](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
- [template base de este template](https://github.com/webcomponents/element-boilerplate/blob/master/my-element.html)
- [reference](https://www.webcomponents.org/introduction)
 - [referencia en español](https://developers.google.com/web/fundamentals/web-components/) con ejemplos
- https://www.codementor.io/ayushgupta/vanilla-js-web-components-chguq8goz
- https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM
- [como hacer templates](https://wesbos.com/template-strings-html) 
- [pruebas automaticas](https://www.npmjs.com/package/web-component-tester)

## notas

### redibujado

En este ejemplo se inyecta en el template el contenido con un slot.

Si queremos repintar todo el contenido de una sola vez tenemos varias opciones.

- utilizar un objeto en vez de datos simples. De ese modo podemos cear el objeto de una sola vez o actualizarlo campo a campo. El usuario del componente elije
- no acutalizar las atributos cuando se hace via setter. no se invocaría la función `attributeChangedCallback` y por tanto habría que mover el codigo de refresco a una funcion llamada `refresh` o `invalidate` que sería llamado por `attributeChangedCallback` en caso de que el usuario optara por modificar los atributos. Este metodo no es común, no es recomendable pues no es intuitivo para el usuario.

### propiedades

para modificar la propiedad country se puede hacer de 3 maneras (todas equivalentes).

la propiedad country se crea automáticament con _createProp(country)

- como propiedad `$0.country="pt"`
- como atributo (via javascript) `$0.setAttribute("country", "es")`
- como atributo (via html) `<component-template country="gb"/>`

## todos
[v] ejecutar con livereload
[v] hello world
[v] crear templates con js
[x] usar templates
[v] propiedades
[v] capturar eventos
[-] funcionando en [v]chrome, [v]firefox, [v]safari, [v]ios, [ ]android, [x]edge, [x]ie11
[v] emitir eventos
[ ] empaquetar
[v] meter los polifills -> presentar algo en shadowdom
[ ] habilitar pruebas automáticas