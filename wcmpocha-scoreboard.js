import * as score from "/node_modules/wcmpocha_score/wcmpocha-score.js"
(async () => {

  class WCMPochaScoreBoard extends HTMLElement {
    constructor() {  
        super();
        this._initialized=false;
    }

    // Getter to let component know what attributes
    // to watch for mutation in this example: country
    static get observedAttributes() {
      return ['country']; 
    };

    static createTemplate(){
      function createStyle(){
        return `
        p {
          color: green;
        }`;
      };
      function createBody(){
        return ` <style>${createStyle()}</style>
        <p> scoreboard </p>
        <ul>
        <ul>`;
      }
      var wrapper = document.createElement("template")
      wrapper.innerHTML = createBody();
      return wrapper;
    }

    static createScoreTemplate(){
      var html = `<li><slot></slot></li>`; 
      var wrapper = document.createElement("template")
      wrapper.innerHTML = html;
      return wrapper;
    }

    render(){
      //this._root.appendChild ( document.importNode(this._template.content, true) );
      this._root.appendChild ( this._template.content.cloneNode(true) );
    }

    _createProp(name , reflect=true, slotable=1){
        self = this;
        const times = n => Array.from(Array(n).keys()) 

        function createSlottedTag (slotIndex) {
          const indexName = (index) => {return index?index:''}   
          var htmlTagAttribute = document.createElement ( "span" );
          htmlTagAttribute.slot = name + indexName(slotIndex) 
          self.appendChild(htmlTagAttribute);
          return htmlTagAttribute;
        }

        function createPropertyStructure (name, reflect, slotable){
          if (self._autoProperties===undefined) self._autoProperties = {};
          self._autoProperties[name]={
            value: undefined,
            slotable: times(slotable).map( createSlottedTag ),
            reflect: reflect
          }
        }

        function createPropertySetterAndGetter(thisObject){
          Object.defineProperty(thisObject, name, {
            set: (value) => {
              var prop = thisObject._autoProperties[name];
              prop.value = value;
              prop.slotable.map(x => x.textContent = value);
              if (prop.reflect) 
                thisObject.setAttribute(name, value); 
            },
            get: () => {
              return thisObject._autoProperties[name].value;
            }
          });
        }

        createPropertyStructure (name, reflect, slotable);
        createPropertySetterAndGetter(this);

    }
    
    fireMyEvent(){
      var payload = {message: "my event!", country: this._countryCode};
      this.dispatchEvent(new CustomEvent('myevent', {detail: payload, bubbles: true}))
    }

    // Fires when an attribute was added, removed, or updated (only for listened in observedAttributes)
    attributeChangedCallback (attr, oldVal, newVal) {
      if (oldVal == newVal) return;
      console.log(`${attr} was changed from ${oldVal} to ${newVal}!`);
      if ("country" == attr) { 
        this.country=newVal;
      }
    }

    //TODO: remove things from template that are not in use
    get players() {return this._players; }
    
    static generateQuickGuid() {
      return Math.random().toString(36).substring(2, 15) +
          Math.random().toString(36).substring(2, 15);
    }

    addPlayer(id, name){
      var self=this;
      function createNewElement(id){
        var newPlayerScore = document.createElement('wcmpocha-score');
        newPlayerScore.id = id;
        newPlayerScore.slot = WCMPochaScoreBoard.generateQuickGuid();
        return newPlayerScore;
      }
      function createSlot(slotname){
        var placer = self._root.querySelector('ul');
        var newSlot = self._scoreTemplate.content.cloneNode(true);
        var slotTag = newSlot.querySelector('slot');
        slotTag.setAttribute('name',slotname);
        placer.append(newSlot);
      }
      function placeElement(element){
        self.appendChild(element)
      }


      var newPlayer = createNewElement(id);
      this._players.push(newPlayer);
      createSlot(newPlayer.slot);
      placeElement(newPlayer);
    }
    removePlayer(){}


    // Fires when an instance of the element is created
    createdCallback () {};
    // Fires when an instance was inserted into the document
    attachedCallback () {};
    // Fires when an instance was removed from the document
    detachedCallback () {};
    // Called when element is inserted in DOM
    connectedCallback() {
      if(this._initialized == false){
        this._initialized = true;
        // define properties
        this._players=[];
        // create shadowdoom
        this._root = this.attachShadow({mode: 'closed'});
        this._template = WCMPochaScoreBoard.createTemplate();
        this._scoreTemplate = WCMPochaScoreBoard.createScoreTemplate();
        // Init sample variable "country"
        this.render();
        // Event handling
        this.addEventListener("click", e => {
          console.log("click!")
          //fire a custom event
          this.fireMyEvent();
        });
        // outside event handling
        // - playerAdd (number, name?)
        window.addEventListener('playerAdd', e => {
          this.addPlayer(e.detail.number, e.detail.name);
        })
        
        this.dispatchEvent(new CustomEvent('ready', {detail: {}, bubbles: true}))
      }
    };
    
  }
  
  customElements.define('wcmpocha-scoreboard', WCMPochaScoreBoard);
})();